from django.db import models

class Movie(models.Model):
    tittle = models.CharField(max_length=48)
    description = models.CharField(max_length=255)
    year = models.IntegerField()
