from rest_framework import viewsets
from rest_framework import permissions
from api.serializers import MovieSerializer
from .models import Movie


class MovieViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Movie.objects.all().order_by('-date_joined')
    serializer_class = MovieSerializer
    permission_classes = [permissions.IsAuthenticated]
